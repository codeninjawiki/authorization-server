package com.codeninja.auth;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestCOntroller {

	@GetMapping(value="/api/check")
	public ResponseEntity<String> test(){
		return new ResponseEntity("UP",HttpStatus.OK);
	}
}
