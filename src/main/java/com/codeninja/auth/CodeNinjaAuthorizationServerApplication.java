package com.codeninja.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@SpringBootApplication
@EnableAuthorizationServer
public class CodeNinjaAuthorizationServerApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(CodeNinjaAuthorizationServerApplication.class, args);
	}

}
